import { Configuration } from '../interfaces/configuration.interface';

export default (): Configuration =>
  ({
    // Default
    [process.env.NODE_ENV]: {
      database: {
        host: process.env.DATABASE_HOST || 'localhost',
        username: process.env.DATABASE_USERNAME || 'postgres',
        password: process.env.DATABASE_PASSWORD || 'docker',
        name: process.env.DATABASE_NAME || 'todoapp-db',
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
        synchronize: Boolean(process.env.DATABASE_SYNCHRONIZE) || true,
      },
      server: {
        port: parseInt(process.env.PORT, 10) || 3000,
      },
    },
    test: {
      database: {
        host: process.env.DATABASE_HOST || 'localhost',
        username: process.env.DATABASE_USERNAME || 'postgres',
        password: process.env.DATABASE_PASSWORD || 'docker',
        name: process.env.DATABASE_NAME || 'todoapp-db-test',
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
        synchronize: Boolean(process.env.DATABASE_SYNCHRONIZE) || true,
      },
      server: {
        port: parseInt(process.env.PORT, 10) || 3001,
      },
    },
    production: {
      database: {
        host: process.env.DATABASE_HOST,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        name: process.env.DATABASE_NAME,
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
        synchronize: Boolean(process.env.DATABASE_SYNCHRONIZE) || false,
      },
      server: {
        port: parseInt(process.env.PORT, 10) || 3002,
      },
    },
  }[process.env.NODE_ENV]);
