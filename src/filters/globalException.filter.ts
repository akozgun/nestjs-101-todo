import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status =
      (<HttpException>exception).getStatus !== undefined
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const timestamp = new Date().toUTCString();
    console.error(exception);

    response.status(status).json({
      message:
        exception.getResponse !== undefined
          ? exception.getResponse()
          : exception.message,
      timestamp,
      path: request.url,
    });
  }
}
