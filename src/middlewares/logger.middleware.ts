import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const requestStart = Date.now();
    let errorMessage = null;
    const body = [];
    let respnseBody = '';

    req.on('data', (chunk) => {
      body.push(chunk);
    });

    req.on('end', () => {
      respnseBody = Buffer.concat(body).toString();
    });

    req.on('error', (error) => {
      errorMessage = error.message;
    });

    req.on('close', () => {
      const { rawHeaders, httpVersion, method, socket, url } = req;
      const { remoteAddress, remoteFamily } = socket;

      const { statusCode, statusMessage } = res;
      const headers = res.getHeaders();

      console.log(
        JSON.stringify({
          timestamp: Date.now(),
          processingTime: Date.now() - requestStart,
          rawHeaders,
          respnseBody,
          errorMessage,
          httpVersion,
          method,
          remoteAddress,
          remoteFamily,
          url,
          response: {
            statusCode,
            statusMessage,
            headers,
          },
        }),
      );
    });

    next();
  }
}
