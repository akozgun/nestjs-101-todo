import { IsString, IsOptional, IsEnum } from 'class-validator';
import { TodoStatus } from '../enums/todo-status.enum';

export class FilterTodoDto {
  @IsOptional()
  @IsString()
  todoName?: string;

  @IsOptional()
  @IsEnum(TodoStatus)
  status?: TodoStatus;
}
