import { PartialType } from '@nestjs/mapped-types';
import { UpdateTodoDto } from './update-todo.dto';

export class ResponseTodoDto extends PartialType(UpdateTodoDto) {
  id: string;
  userId: string;
  dateCreated: Date;
  dateLastUpdated: Date;
}
