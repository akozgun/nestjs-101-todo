import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { TodoStatus } from '../enums/todo-status.enum';
export class UpdateTodoDto {
  @IsOptional()
  @IsNotEmpty()
  todoName: string;

  @IsOptional()
  @IsEnum(TodoStatus)
  status: TodoStatus;
}
