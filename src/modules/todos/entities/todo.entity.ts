import { AuditableEntity } from '../../../common/entities/auditable.entity';
import { Column, Entity, ManyToOne, JoinColumn } from 'typeorm';
import { TodoStatus } from '../enums/todo-status.enum';
import { User } from '../../../modules/users/entities/user.entity';

@Entity()
export class Todo extends AuditableEntity {
  @Column({ type: 'varchar' })
  todoName: string;
  @Column({ type: 'integer', default: TodoStatus.NOT_STARTED })
  status: TodoStatus;
  @Column({ type: 'uuid' })
  userId: string;

  @ManyToOne(() => User, (user) => user.todos)
  @JoinColumn({ name: 'userId' })
  user: User;
}
