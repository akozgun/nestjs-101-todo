import { User } from '../../../modules/users/entities/user.entity';
import { CreateTodoDto } from '../dto/create-todo.dto';
import { FilterTodoDto } from '../dto/filter-todo.dto';
import { ResponseTodoDto } from '../dto/response-todo.dto';
import { UpdateTodoDto } from '../dto/update-todo.dto';

export interface ITodoServie {
  getTodos(
    user: User,
    filterTodoDto: FilterTodoDto,
  ): Promise<ResponseTodoDto[]>;
  getTodoById(user: User, id: string): Promise<ResponseTodoDto>;
  createTodo(
    user: User,
    createTaskDTO: CreateTodoDto,
  ): Promise<ResponseTodoDto>;
  deleteTodoById(user: User, id: string): Promise<boolean>;
  updateTodo(
    id: string,
    user: User,
    updateTaskDTO: UpdateTodoDto,
  ): Promise<ResponseTodoDto>;
}
