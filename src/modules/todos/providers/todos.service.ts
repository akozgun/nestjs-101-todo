import { Injectable } from '@nestjs/common';
import { CreateTodoDto } from '../dto/create-todo.dto';
import { ResponseTodoDto } from '../dto/response-todo.dto';
import { UpdateTodoDto } from '../dto/update-todo.dto';
import { ITodoServie } from '../interfaces/todo-service.interface';
import { User } from '../../users/entities/user.entity';
import { FilterTodoDto } from '../dto/filter-todo.dto';
import { TodoRepository } from '../repositories/todo.respository';

@Injectable()
export class TodosService implements ITodoServie {
  constructor(private todoRepository: TodoRepository) {}

  async getTodos(
    user: User,
    filterTodoDto: FilterTodoDto,
  ): Promise<ResponseTodoDto[]> {
    return this.todoRepository.getTodos(user, filterTodoDto);
  }

  async getTodoById(user: User, id: string): Promise<ResponseTodoDto> {
    const todo = await this.todoRepository.findOne({ where: { id, user } });
    if (!todo) return undefined;
    return this.todoRepository.buildResponseTodoDto(todo);
  }

  async createTodo(
    user: User,
    createTodoDto: CreateTodoDto,
  ): Promise<ResponseTodoDto> {
    return this.todoRepository.createTodo(user, createTodoDto);
  }

  async deleteTodoById(user: User, id: string): Promise<boolean> {
    const result = await this.todoRepository.delete({ id, userId: user.id });
    return result && result.affected == 1;
  }

  async updateTodo(
    id: string,
    user: User,
    updateTodoDto: UpdateTodoDto,
  ): Promise<ResponseTodoDto> {
    const todo = await this.getTodoById(user, id);
    if (!todo) return undefined;
    return this.todoRepository.updateTodo(id, updateTodoDto);
  }
}
