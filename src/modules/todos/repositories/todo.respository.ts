import { Repository, EntityRepository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';
import { Todo } from '../entities/todo.entity';
import { CreateTodoDto } from '../dto/create-todo.dto';
import { FilterTodoDto } from '../dto/filter-todo.dto';
import { User } from '../../users/entities/user.entity';
import { ResponseTodoDto } from '../dto/response-todo.dto';
import { UpdateTodoDto } from '../dto/update-todo.dto';

@EntityRepository(Todo)
export class TodoRepository extends Repository<Todo> {
  async createTodo(
    user: User,
    createTaskDTO: CreateTodoDto,
  ): Promise<ResponseTodoDto> {
    const partialTask = this.create(createTaskDTO);
    partialTask.user = user;

    try {
      const task = await this.save(partialTask);
      return this.buildResponseTodoDto(task);
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async getTodos(
    user: User,
    filterTodoDto: FilterTodoDto,
  ): Promise<ResponseTodoDto[]> {
    const { status, todoName } = filterTodoDto;

    const query = this.createQueryBuilder('todo');

    query.where('todo.userId = :userId', { userId: user.id });

    if (status != undefined) {
      query.andWhere('todo.status = :status', { status });
    }

    if (todoName) {
      query.andWhere('(todo.todoName LIKE :searchTerm)', {
        searchTerm: `%${todoName}%`,
      });
    }

    return query.getMany();
  }

  async updateTodo(
    id: string,
    updateTodoDto: UpdateTodoDto,
  ): Promise<ResponseTodoDto> {
    await this.save({ id, ...updateTodoDto });
    const todo = await this.findOne(id);
    return this.buildResponseTodoDto(todo);
  }

  buildResponseTodoDto(todo: Todo): ResponseTodoDto {
    const { id, status, todoName, userId, dateCreated, dateLastUpdated } = todo;

    return {
      id,
      status,
      todoName,
      userId,
      dateCreated,
      dateLastUpdated,
    };
  }
}
