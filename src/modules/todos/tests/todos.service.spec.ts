import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../../../modules/users/entities/user.entity';
import { FilterTodoDto } from '../dto/filter-todo.dto';
import { ResponseTodoDto } from '../dto/response-todo.dto';
import { TodoStatus } from '../enums/todo-status.enum';
import { TodosService } from '../providers/todos.service';
import { TodoRepository } from '../repositories/todo.respository';

describe('TodosService', () => {
  let sut: TodosService;
  let repository: TodoRepository;
  let mockTodoResponse: ResponseTodoDto[];
  let mockFilterDto: FilterTodoDto;
  let mockUser = new User();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TodosService, TodoRepository],
    }).compile();

    sut = module.get<TodosService>(TodosService);
    repository = module.get<TodoRepository>(TodoRepository);

    mockTodoResponse = [
      {
        id: '123',
        todoName: 'mockTitle',
        status: TodoStatus.IN_PROGRESS,
        userId: 'mockUserId',
        dateCreated: new Date('10-11-2021'),
        dateLastUpdated: new Date('10-10-2021'),
      },
      {
        id: '124',
        todoName: 'mockTestTitle',
        status: TodoStatus.NOT_STARTED,
        userId: 'mockUserId',
        dateCreated: new Date('10-10-2021'),
        dateLastUpdated: new Date('10-10-2021'),
      },
      {
        id: '125',
        todoName: 'mockTestTitle',
        status: TodoStatus.IN_PROGRESS,
        userId: 'mockUserId',
        dateCreated: new Date('10-10-2021'),
        dateLastUpdated: new Date('10-10-2021'),
      },
    ];

    mockFilterDto = {
      todoName: 'test',
      status: TodoStatus.IN_PROGRESS,
    };

    mockUser = new User();
  });

  it('Should be defined', () => {
    expect(sut).toBeDefined();
    expect(repository).toBeDefined();
  });

  describe('Find all function', () => {
    it('Should get all todo items', async () => {
      repository.getTodos = jest.fn().mockResolvedValue(mockTodoResponse);
      const response = await sut.getTodos(mockUser, {});
      expect(repository.getTodos).toHaveBeenCalledWith(mockUser, {});
      expect(response).toEqual(mockTodoResponse);
    });

    it('it should retrive only in progress items', async () => {
      const filteredValue: ResponseTodoDto[] = mockTodoResponse.filter(
        (todo) =>
          todo.status === TodoStatus.IN_PROGRESS &&
          todo.todoName.indexOf('test') !== -1,
      );

      repository.getTodos = jest.fn().mockResolvedValue(filteredValue);
      const response = await sut.getTodos(mockUser, mockFilterDto);

      expect(repository.getTodos).toHaveBeenCalledWith(mockUser, mockFilterDto);
      expect(response).toEqual(filteredValue);
    });
  });
});
