import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  ParseUUIDPipe,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { ResponseTodoDto } from './dto/response-todo.dto';
import { TodosService } from './providers/todos.service';
import { FilterTodoDto } from './dto/filter-todo.dto';
import { User } from '../users/entities/user.entity';
import { GetUser } from '../users/decorators/getUser.decorator';
import { AuthGuard } from '@nestjs/passport';
import { ResponseTransformInterceptor } from '../../interceptors/response-transform.interceptor';

@Controller('todos')
@UseGuards(AuthGuard('jwt'))
@UseInterceptors(ResponseTransformInterceptor)
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @Post()
  async create(
    @GetUser() user: User,
    @Body() createTodoDto: CreateTodoDto,
  ): Promise<ResponseTodoDto> {
    return this.todosService.createTodo(user, createTodoDto);
  }

  @Get()
  async findAll(
    @GetUser() user: User,
    @Body() filterTodoDto: FilterTodoDto,
  ): Promise<ResponseTodoDto[]> {
    return this.todosService.getTodos(user, filterTodoDto);
  }

  @Get(':id')
  async findOne(
    @GetUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<ResponseTodoDto> {
    const todo = await this.todosService.getTodoById(user, id);
    if (!todo) throw new NotFoundException();
    return todo;
  }

  @Patch(':id')
  async update(
    @GetUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() updateTodoDto: UpdateTodoDto,
  ): Promise<ResponseTodoDto> {
    const todo = await this.todosService.updateTodo(id, user, updateTodoDto);
    if (!todo) throw new NotFoundException();
    return todo;
  }

  @Delete(':id')
  async remove(
    @GetUser() user: User,
    @Param('id', new ParseUUIDPipe()) id: string,
  ): Promise<boolean> {
    const todo = await this.todosService.deleteTodoById(user, id);
    if (!todo) throw new NotFoundException();
    return todo;
  }
}
