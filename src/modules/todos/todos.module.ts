import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { TodosService } from './providers/todos.service';
import { TodoRepository } from './repositories/todo.respository';
import { TodosController } from './todos.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TodoRepository]), UsersModule],
  controllers: [TodosController],
  providers: [TodosService],
})
export class TodosModule {}
