import { OmitType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
export class ResponseUserDto extends OmitType(CreateUserDto, ['password']) {
  id: string;
  dateCreated: Date;
}
