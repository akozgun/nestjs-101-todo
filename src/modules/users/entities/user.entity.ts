import { AuditableEntity } from '../../../common/entities/auditable.entity';
import { Column, Entity, OneToMany, Unique } from 'typeorm';
import { Todo } from '../../../modules/todos/entities/todo.entity';

@Entity()
@Unique(['username'])
export class User extends AuditableEntity {
  @Column({ type: 'varchar', length: 256 })
  username: string;
  @Column()
  password: string;
  @Column()
  salt: string;

  @OneToMany(() => Todo, (todo) => todo.user)
  todos: Todo[];
}
