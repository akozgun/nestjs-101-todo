import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from '../repositories/user.repository';
import { CreateUserDto } from '../dto/create-user.dto';
import { ResponseUserDto } from '../dto/response-user.dto';
import { SignInUserDto } from '../dto/signin-user.dto';
import { AccessToken } from '../dto/access-token.dto';

@Injectable()
export class UsersService {
  constructor(
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async createUser(createUserDTO: CreateUserDto): Promise<ResponseUserDto> {
    return this.userRepository.createUser(createUserDTO);
  }

  async signInUser(SignInUserDto: SignInUserDto): Promise<AccessToken> {
    const jwtPayload = await this.userRepository.signInUser(SignInUserDto);
    const bearerToken = await this.jwtService.signAsync(jwtPayload);
    return { bearerToken };
  }
}
