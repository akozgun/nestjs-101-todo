import { EntityRepository, Repository } from 'typeorm';
import { genSalt, hash } from 'bcrypt';
import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../dto/create-user.dto';
import { ResponseUserDto } from '../dto/response-user.dto';
import { SignInUserDto } from '../dto/signin-user.dto';
import { JwtPayload } from '../interfaces/jwt-payload.interface';

@Injectable()
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createUserDTO: CreateUserDto): Promise<ResponseUserDto> {
    const { username, password } = createUserDTO;

    const result = await this.hashPassword(password);

    const partialUser = this.create({
      username,
      password: result.hashedPassword,
      salt: result.salt,
    });

    try {
      const user = await partialUser.save();

      return {
        id: user.id,
        username: user.username,
        dateCreated: user.dateCreated,
      };
    } catch (error) {
      if (error.code === '23505') {
        const errorMessage = `User with username ${username} already exists`;
        throw new ConflictException(errorMessage);
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async signInUser(signInUserDTO: SignInUserDto): Promise<JwtPayload> {
    const { username, password } = signInUserDTO;
    const errorMessage = 'Username or password is wrong.';

    const user = await this.findOne({ username });

    if (!user) {
      throw new UnauthorizedException(errorMessage);
    }

    const comparePasswordResult = await this.validatePassword(password, user);

    if (!comparePasswordResult) {
      throw new UnauthorizedException(errorMessage);
    }

    return { username: user.username };
  }

  private async hashPassword(
    password: string,
  ): Promise<{ hashedPassword: string; salt: string }> {
    const salt = await genSalt();
    const hashedPassword = await hash(password, salt);
    return { hashedPassword, salt };
  }

  private async validatePassword(
    inputPassword: string,
    user: User,
  ): Promise<boolean> {
    const inputPasswordHash = await hash(inputPassword, user.salt);

    if (inputPasswordHash === user.password) return true;

    return false;
  }
}
