import { Controller, Post, Body } from '@nestjs/common';
import { AccessToken } from './dto/access-token.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { ResponseUserDto } from './dto/response-user.dto';
import { SignInUserDto } from './dto/signin-user.dto';
import { UsersService } from './providers/users.service';

@Controller('users')
export class UsersController {
  constructor(private authService: UsersService) {}

  @Post('/signup')
  async signup(@Body() createUserDTO: CreateUserDto): Promise<ResponseUserDto> {
    return this.authService.createUser(createUserDTO);
  }

  @Post('/signin')
  async signin(@Body() signInUserDTO: SignInUserDto): Promise<AccessToken> {
    return this.authService.signInUser(signInUserDTO);
  }
}
