import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { TodosModule } from '../src/modules/todos/todos.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigService } from '../src/config/typeorm.config';
import configuration from '../src/config/configuration';
import { UsersModule } from '../src/modules/users/users.module';
import { TodoRepository } from '../src/modules/todos/repositories/todo.respository';
import { UserRepository } from '../src/modules/users/repositories/user.repository';
import { CreateUserDto } from '../src/modules/users/dto/create-user.dto';
import { TodoStatus } from '../src/modules/todos/enums/todo-status.enum';

describe('TodosController (e2e)', () => {
  let app: INestApplication;
  let moduleFixture: TestingModule;
  let todoRepository: TodoRepository;
  let userRepository: UserRepository;
  let bearerToken: string;
  let userId: string;

  beforeAll(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({ isGlobal: true, load: [configuration] }),
        TypeOrmModule.forRootAsync({ useClass: TypeOrmConfigService }),
        TodosModule,
        UsersModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    todoRepository = moduleFixture.get<TodoRepository>(TodoRepository);
    userRepository = moduleFixture.get<UserRepository>(UserRepository);

    await todoRepository.clear();
    await userRepository.delete({});
    const user: CreateUserDto = { username: 'test', password: 'password' };
    const result = await userRepository.createUser(user);
    userId = result.id;

    bearerToken = (
      await request(app.getHttpServer()).post('/users/signin').send(user)
    ).body.bearerToken;
  });

  beforeEach(async () => {
    await todoRepository.clear();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('/todos (GET)', () => {
    it('should return 401 (Unauthroized) without token', () => {
      return request(app.getHttpServer())
        .get('/todos')
        .expect(HttpStatus.UNAUTHORIZED);
    });

    it('should return 200 (OK) with token', () => {
      return request(app.getHttpServer())
        .get('/todos')
        .set('Authorization', `Bearer ${bearerToken}`)
        .expect(HttpStatus.OK);
    });

    it('should return data', async () => {
      const todoItem1 = await todoRepository.save({
        todoName: 'new todo',
        status: TodoStatus.NOT_STARTED,
        userId: userId,
      });

      const todoItem2 = await todoRepository.save({
        todoName: 'another new todo',
        status: TodoStatus.NOT_STARTED,
        userId: userId,
      });

      const expected = [{ ...todoItem1 }, { ...todoItem2 }];

      return request(app.getHttpServer())
        .get('/todos')
        .set('Authorization', `Bearer ${bearerToken}`)
        .then((response) => {
          expect(200);
          expect(response.body.data).toEqual(
            JSON.parse(JSON.stringify(expected)),
          );
        });
    });
  });
});
